package br.ufrn.imd.universidade_application.enuns;

public enum Genero {
    FEMININO,
    MASCULINO,
    OUTRO
}
