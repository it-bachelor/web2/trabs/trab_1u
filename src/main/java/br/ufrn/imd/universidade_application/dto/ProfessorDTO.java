package br.ufrn.imd.universidade_application.dto;

import br.ufrn.imd.universidade_application.enuns.Genero;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.Min;
import org.hibernate.validator.constraints.br.CPF;
import org.springframework.format.annotation.DateTimeFormat;
import java.time.LocalDate;

public record ProfessorDTO(
        String nome,

        @CPF(message = "CPF invalido")
        String cpf,

        @Min(value=1, message = "Matricula é obrigatória")
        int matricula,

        @Enumerated(EnumType.STRING)
        Genero genero,

        String departamento,

        @DateTimeFormat
        LocalDate dataNascimento,

        @Min(0)
        float salario,

        String disciplinaAssociada
        ) {
}
