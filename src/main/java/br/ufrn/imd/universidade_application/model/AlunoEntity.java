package br.ufrn.imd.universidade_application.model;

import br.ufrn.imd.universidade_application.dto.AlunoDTO;
import br.ufrn.imd.universidade_application.enuns.Genero;
import jakarta.persistence.*;
import lombok.*;
import java.time.LocalDate;

@Entity
@Table(name="alunos")
@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AlunoEntity {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nome;
    private String cpf;
    private int matricula;
    private Genero genero;
    private String curso;
    private LocalDate dataNascimento;

    @Column(columnDefinition = "BOOLEAN DEFAULT true")
    private boolean ativo;

    public AlunoEntity(AlunoDTO aluno){
        this.nome = aluno.nome();
        this.cpf = aluno.cpf();
        this.matricula = aluno.matricula();
        this.genero = aluno.genero();
        this.curso = aluno.curso();
        this.dataNascimento = aluno.dataNascimento();
        this.ativo = true;
    }

    public void inativarAluno() {
        this.ativo = false;
    }

    public void ativarAluno() {
        this.ativo = true;
    }
    
    public void atualizarAlunoParcialmente(AlunoDTO aluno) {
        if (aluno.nome() != null && !aluno.nome().isEmpty()) this.nome = aluno.nome();
        if (aluno.cpf() != null && !aluno.cpf().isEmpty()) this.cpf = aluno.cpf();
        this.matricula = aluno.matricula();
        if (aluno.genero() != null) this.genero = aluno.genero();
        if (aluno.curso() != null && !aluno.curso().isEmpty()) this.curso = aluno.curso();
        if (aluno.dataNascimento() != null) this.dataNascimento = aluno.dataNascimento();
    }
}
