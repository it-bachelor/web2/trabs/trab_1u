package br.ufrn.imd.universidade_application.model;

import br.ufrn.imd.universidade_application.dto.ProfessorDTO;
import br.ufrn.imd.universidade_application.enuns.Genero;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;

@Entity
@Table(name="professores")
@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProfessorEntity {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nome;
    private int matricula;
    private Genero genero;
    private String departamento;
    private LocalDate dataNascimento;
    private float salario;
    private String disciplinaAssociada;

    @Column(columnDefinition = "BOOLEAN DEFAULT true")
    private boolean ativo;

    public ProfessorEntity(ProfessorDTO professor){
        this.nome = professor.nome();
        this.matricula = professor.matricula();
        this.genero = professor.genero();
        this.departamento = professor.departamento();
        this.dataNascimento = professor.dataNascimento();
        this.salario = professor.salario();
        this.disciplinaAssociada = professor.disciplinaAssociada();
        this.ativo = true;
    }

    public void inativarProfessor() {
        this.ativo = false;
    }

    public void ativarProfessor() {
        this.ativo = true;
    }

    public void atualizarProfessorParcialmente(ProfessorDTO professor) {
        if (professor.nome() != null && !professor.nome().isEmpty()) this.nome = professor.nome();
        this.matricula = professor.matricula();
        if (professor.genero() != null) this.genero = professor.genero();
        if (professor.departamento() != null && !professor.departamento().isEmpty()) this.departamento = professor.departamento();
        if (professor.dataNascimento() != null) this.dataNascimento = professor.dataNascimento();
        if (professor.salario() > 0.0f) this.salario = professor.salario();
        if (professor.disciplinaAssociada() != null && !professor.disciplinaAssociada().isEmpty()) this.disciplinaAssociada = professor.disciplinaAssociada();
    }
}
