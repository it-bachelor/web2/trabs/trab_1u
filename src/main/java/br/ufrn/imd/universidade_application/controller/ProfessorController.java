package br.ufrn.imd.universidade_application.controller;

import br.ufrn.imd.universidade_application.dto.ProfessorDTO;
import br.ufrn.imd.universidade_application.model.ProfessorEntity;
import br.ufrn.imd.universidade_application.repository.ProfessorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/professores")
public class ProfessorController {
    @Autowired
    private ProfessorRepository repository;

    @PostMapping
    public ResponseEntity<ProfessorEntity> postProfessor(@RequestBody @Valid ProfessorDTO professor){
        ProfessorEntity professorEntity = new ProfessorEntity(professor);
        ProfessorEntity createdProfessor = repository.save(professorEntity);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdProfessor);
    }

    @GetMapping("/list")
    public ResponseEntity<List<ProfessorEntity>> getAll(){
        List<ProfessorEntity> foundActiveProfessors = repository.findAllByAtivoTrue();
        return ResponseEntity.status(HttpStatus.OK).body(foundActiveProfessors);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProfessorEntity> getById(@PathVariable(value = "id") Long id) {
        Optional<ProfessorEntity> professorEntity = repository.findById(id);
        if (professorEntity.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        ProfessorEntity foundProfessor = professorEntity.get();
        if (!foundProfessor.isAtivo()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.status(HttpStatus.OK).body(foundProfessor);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<ProfessorEntity> deleteProfessor(@PathVariable(value = "id") Long id) {
        Optional<ProfessorEntity> professorEntity = repository.findById(id);
        if (professorEntity.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        ProfessorEntity professorToDelete = professorEntity.get();
        repository.delete(professorToDelete);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @DeleteMapping("/inactivate/{id}")
    public ResponseEntity<ProfessorEntity> deleteLogic(@PathVariable(value = "id") Long id) {
        Optional<ProfessorEntity> professorEntity = repository.findById(id);
        if (professorEntity.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        ProfessorEntity professorToInactivate = professorEntity.get();
        professorToInactivate.inativarProfessor();
        repository.save(professorToInactivate);
        return ResponseEntity.status(HttpStatus.OK).body(professorToInactivate);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ProfessorEntity> putProfessor(@PathVariable(value = "id") Long id, @RequestBody @Valid ProfessorDTO professor){
        Optional<ProfessorEntity> professorEntity = repository.findById(id);
        if(professorEntity.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        ProfessorEntity existingProfessor = professorEntity.get();
        if (!existingProfessor.isAtivo()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        existingProfessor.atualizarProfessorParcialmente(professor);
        ProfessorEntity savedProfessor = repository.save(existingProfessor);
        return ResponseEntity.status(HttpStatus.OK).body(savedProfessor);
    }

}
