package br.ufrn.imd.universidade_application.controller;

import br.ufrn.imd.universidade_application.dto.AlunoDTO;
import br.ufrn.imd.universidade_application.model.AlunoEntity;
import br.ufrn.imd.universidade_application.repository.AlunoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/alunos")
public class AlunoController {
    @Autowired
    private AlunoRepository repository;

    @PostMapping
    public ResponseEntity<AlunoEntity> postAluno(@RequestBody @Valid AlunoDTO aluno){
        AlunoEntity alunoEntity = new AlunoEntity(aluno);
        AlunoEntity createdAluno = repository.save(alunoEntity);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdAluno);
    }

    @GetMapping("/list")
    public ResponseEntity<List<AlunoEntity>> getAll(){
        List<AlunoEntity> foundActiveAlunos = repository.findAllByAtivoTrue();
        return ResponseEntity.status(HttpStatus.OK).body(foundActiveAlunos);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AlunoEntity> getById(@PathVariable(value = "id") Long id) {
        Optional<AlunoEntity> alunoEntity = repository.findById(id);
        if (alunoEntity.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        AlunoEntity foundAluno = alunoEntity.get();
        if (!foundAluno.isAtivo()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.status(HttpStatus.OK).body(foundAluno);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<AlunoEntity> deleteAluno(@PathVariable(value = "id") Long id) {
        Optional<AlunoEntity> alunoEntity = repository.findById(id);
        if (alunoEntity.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        AlunoEntity alunoToDelete = alunoEntity.get();
        repository.delete(alunoToDelete);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @DeleteMapping("/inactivate/{id}")
    public ResponseEntity<AlunoEntity> deleteLogic(@PathVariable(value = "id") Long id) {
        Optional<AlunoEntity> alunoEntity = repository.findById(id);
        if (alunoEntity.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        AlunoEntity alunoToInactivate = alunoEntity.get();
        alunoToInactivate.inativarAluno();
        repository.save(alunoToInactivate);
        return ResponseEntity.status(HttpStatus.OK).body(alunoToInactivate);
    }

    @PutMapping("/{id}")
    public ResponseEntity<AlunoEntity> putAluno(@PathVariable(value = "id") Long id, @RequestBody @Valid AlunoDTO aluno){
        Optional<AlunoEntity> alunoEntity = repository.findById(id);
        if(alunoEntity.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        AlunoEntity existingAluno = alunoEntity.get();
        if (!existingAluno.isAtivo()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        existingAluno.atualizarAlunoParcialmente(aluno);
        AlunoEntity savedAluno = repository.save(existingAluno);
        return ResponseEntity.status(HttpStatus.OK).body(savedAluno);
    }

}
