package br.ufrn.imd.universidade_application.repository;

import br.ufrn.imd.universidade_application.model.AlunoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface AlunoRepository extends JpaRepository<AlunoEntity, Long> {
    List<AlunoEntity> findAllByAtivoTrue();
}
