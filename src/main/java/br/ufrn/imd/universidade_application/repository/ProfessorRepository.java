package br.ufrn.imd.universidade_application.repository;

import br.ufrn.imd.universidade_application.model.ProfessorEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ProfessorRepository extends JpaRepository<ProfessorEntity, Long> {
    List<ProfessorEntity> findAllByAtivoTrue();
}
