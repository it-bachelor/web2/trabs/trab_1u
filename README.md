# 0. dependências
Para executar este projeto, será necessário:
- linux ubuntu (aqui é apenas recomendação, mas teoricamente funcionaria, apesar de alguns cenários do docker ter inconsistências no windows)
- algum ambiente de interpretação de comandos shell (usei bash)
- java jdk 21, ambiente da aplicação criada
- alguma ide com java configurado (usei intellij idea ce), para executar a aplicação java c/ spring
- maven, para gerenciar dependências e executar a aplicação
- hardware com virtualização habilitada, para executar o docker
- docker engine (vide item 2 abaixo), para executar containers configurados
- docker compose plugin (vide item 3 abaixo), para subir o ambiente conjunto, do sgbd e do cliente sql p/ acessar o sgbd
- qualquer browser
- bruno rest client (vide item 1 abaixo), para testar manualmente as requisições http

# 1. instalando o bruno rest client
No ubuntu:
- `snap install bruno`
- outras opções (inclusive no formato portátil do AppImage), vide: https://www.usebruno.com/downloads

# 2. instalando docker engine
- https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository
- https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user

# 3. instalando docker compose plugin
- https://docs.docker.com/compose/install/linux/#install-using-the-repository

# 4. bind dos volumes do mysql no diretorio local
Essa etapa é opcional, caso queira controlar o uso de disco do mysql, ou fazer backup dos dados. Caso seja sua opção, deve habilitar os trechos desativados no arquivo do `docker-compose.yml`.

Se escolhê-la, pode ainda optar por ter controle sobre os dados gerados do mysql. Para isso, uma configuração de owner e group do diretório de dados do mysql deve ser feita após subir o `docker-compose` (etapa seguinte).
```bash
sudo chown -R $USER:$USER ./mysql_data
```
**OBS**: o diretório `mysql_data` será o que o `docker-compose` criará quando subir o ambiente com bind de volume para o container do mysql.

# 5. executando o ambiente sgbd

## a) se o `spring-boot-docker-compose` não estiver ativado no `pom.xml`
Estando no diretório do projeto, execute o comando abaixo para subir o ambiente:
```bash
docker-compose up -d
```

Para encerrar este ambiente do sgbd:
```bash
docker-compose down
```

## b) se o `spring-boot-docker-compose` estiver ativado no `pom.xml`
Basta apenas executar a aplicação spring boot, que o ambiente do sgbd será subido pelo próprio spring.

Vide refs:
- https://spring.io/blog/2023/06/21/docker-compose-support-in-spring-boot-3-1
- https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#features.docker-compose

# 6. acesso ao sql client `phpMyAdmin`
Após uns segundos de subir o ambiente, acessar na máquina host:
```
http://localhost:3000
```

Será exibida a tela inicial do phpMyAdmin.

- Em server, coloque o nome do servidor de SGBD registrado no DNS da rede virtual criada pelo docker-compose, ou seja: `mysql`.
- em username, coloque `root` ou o valor que está na veriável `MYSQL_USER`.
- em senha, o valor que está na variável `MYSQL_ROOT_PW` (para o root) ou em `MYSQL_USER_PW` (para o user de `MYSQL_USER`).

# 7. limpando seu histórico de containers
Após encerrar o uso desta aplicação, para evitar que os containers aqui usados fiquem no histórico de nomes de containers encerrados no seu ambiente, utilize:
```bash
docker system prune
```

# 8. bug bruno
- https://github.com/usebruno/bruno/issues/2215

P/ validar uma request c/ pre request variable, por enquanto está com bug de ter que requisitar 2 vezes.

# 9. apresentação
- [vídeo 1](https://youtu.be/_vuf5G7Q6o4)
- [vídeo 2](https://youtu.be/LqVocr-yVfg)
- [repo](https://gitlab.com/it-bachelor/web2/trabs/trab_1u)